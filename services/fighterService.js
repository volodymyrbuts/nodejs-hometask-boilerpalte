const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    get() {
        return FighterRepository.getAll()
    }

    create(fighterData) {
        return FighterRepository.create(fighterData)
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, fighterData) {
        return FighterRepository.update(id, fighterData)
    }

    delete(id) {
        return FighterRepository.delete(id)
    }
}

module.exports = new FighterService();