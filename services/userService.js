const { UserRepository } = require('../repositories/userRepository');

class UserService {

    create(userData) {
        return UserRepository.create(userData)
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
    get() {
        return UserRepository.getAll()
    }

    update(id, userData) {
        return UserRepository.update(id, userData)
    }

    delete(id) {
        return UserRepository.delete(id)
    }
}

module.exports = new UserService();