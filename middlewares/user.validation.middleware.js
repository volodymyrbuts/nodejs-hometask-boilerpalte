const { user } = require('../models/user');
const UserService = require('../services/userService');

const [id, ...userWithoutId] = Object.keys(user)

const emptyName = (req, res) => {
    if (!req.body.firstName || !req.body.lastName) {
        throw res.status(400).json({
            error: true,
            message: 'Name shouldn\'t be empty'
        })
    }
}

const wrongEmail = (req, res) => {
    if (!req.body.email || !req.body.email.includes('@gmail.com')) {
        throw (
            res.status(400).json({
                error: true,
                message: 'You should use gmail'
            }))
    }
}

const sameEmail = (req, res) => {
    if (UserService.search({ email: req.body.email })) {
        throw (
            res.status(400).json({
                error: true,
                message: 'User with that email already exists'
            }))
    }
}
const samePhone = (req, res) => {
    if (UserService.search({ phoneNumber: req.body.phoneNumber })) {
        throw (
            res.status(400).json({
                error: true,
                message: 'User with that phone number already exists'
            }))
    }
}

const sameEmailUpdate = (req, res) => {
    const item = UserService.search({ email: req.body.email })
    if (item && item.id != req.params.id) {
        throw (
            res.status(400).json({
                error: true,
                message: 'User with that email already exists'
            }))
    }
}
const samePhoneNumberUpdate = (req, res) => {
    const item = UserService.search({ phoneNumber: req.body.phoneNumber })
    if (item && item.id != req.params.id) {
        throw (
            res.status(400).json({
                error: true,
                message: 'User with that phone number already exists'
            }))
    }
}
const wrongPhone = (req, res) => {
    if (!req.body.phoneNumber || !req.body.phoneNumber.includes('+380') || req.body.phoneNumber.length < 13) {
        throw (res.status(400).json({
            error: true,
            message: 'Phone scould start from +380 and be valid'
        }))
    }
}

const wrongPass = (req, res) => {
    if (!req.body.password || req.body.password.length < 3) {
        throw (res.status(400).json({
            error: true,
            message: 'Min password length 3'
        }))
    }
}

const wrongId = (req, res) => {
    if (!UserService.search(req.params)) {
        throw (
            res.status(404).json({
                error: true,
                message: 'User with that id not find'
            })
        )
    }
}
const rightProperties = (req, res) => {
    const hasAllRequiredFields = userWithoutId.every(key =>
        req.body.hasOwnProperty(key))
    const hasOnlyAllowedFields = Object.keys(req.body).every(key =>
        userWithoutId.includes(key)
    )
    if (!(hasAllRequiredFields
        && hasOnlyAllowedFields)) {
        throw (res.status(400).json({
            error: true,
            message: 'Validation error'
        }))
    }
}


const createUserValid = (req, res, next) => {
    try {
        emptyName(req, res)
        wrongEmail(req, res)
        sameEmail(req, res)
        samePhone(req, res)
        wrongPhone(req, res)
        wrongPass(req, res)
        rightProperties(req, res)
        next()
    } catch (err) {
        res.err = err;
    }
}

const updateUserValid = (req, res, next) => {
    try {
        // emptyName(req, res)
        wrongEmail(req, res)
        // samePhoneNumberUpdate(req, res)
        // sameEmailUpdate(req, res)
        wrongPhone(req, res)
        wrongPass(req, res)
        rightProperties(req, res)
        wrongId(req, res)
        next()
    } catch (err) {
        res.err = err;
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;