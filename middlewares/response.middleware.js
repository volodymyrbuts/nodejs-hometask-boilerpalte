const responseMiddleware = (req, res, next) => {

    if (res.data) {
        return res.status(200).json(res.data)
    }

    if (res.err) {
        return res.json({
            error: true,
            message: res.err,
        })
    };

    next();
}

exports.responseMiddleware = responseMiddleware;