const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const [id, ...fighterWithoutId] = Object.keys(fighter)

const emptyName = (req, res) => {
    if (!req.body.name) {
        throw res.status(400).json({
            error: true,
            message: 'Name shouldn`t be empty'
        })
    }
}

const wrongValue = (req, res) => {
    const isPowerWrong = (req.body.power < 1 || req.body.power > 100 || Number.isNaN(Number(req.body.power)))
    const isDefenseWrong = (req.body.defense < 1 || req.body.defense > 100 || Number.isNaN(Number(req.body.defense)))
    const isHealthWrong = (req.body.health < 1 || req.body.health > 100 || Number.isNaN(Number(req.body.health)))
    if (isPowerWrong || isDefenseWrong || isHealthWrong) {
        throw res.status(400).json({
            error: true,
            message: 'Wrong value'
        })
    }
}

const sameName = (req, res) => {
    if (FighterService.search({ name: req.body.name })) {
        throw (
            res.status(400).json({
                error: true,
                message: 'Fighter with that name already exists'
            })
        )
    }
}

const sameNameUpdate = (req, res) => {
    const item = FighterService.search({ name: req.body.name })
    if (item && item.id != req.params.id) {
        throw (
            res.status(400).json({
                error: true,
                message: 'Fighter with that name already exists'
            })
        )
    }
}

const wrongId = (req, res) => {
    if (!FighterService.search({ id: req.params.id })) {
        throw (
            res.status(404).json({
                error: true,
                message: 'Fighter with that id is not found'
            })
        )
    }
}

const rightProperties = (req, res) => {
    const hasAllRequiredFields = fighterWithoutId.every(key =>
        req.body.hasOwnProperty(key))
    const hasOnlyAllowedFields = Object.keys(req.body).every(key =>
        fighterWithoutId.includes(key)
    )
    if (!(hasAllRequiredFields
        && hasOnlyAllowedFields)) {
        throw (res.status(400).json({
            error: true,
            message: 'Validation error'
        }))
    }
}

const createFighterValid = (req, res, next) => {
    try {
        sameName(req, res)
        emptyName(req, res)
        wrongValue(req, res)
        rightProperties(req, res)
        next()
    } catch (err) {
        res.err = err;
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        // sameNameUpdate(req, res)
        // emptyName(req, res)
        rightProperties(req, res)
        wrongValue(req, res)
        wrongId(req, res)
        next()
    } catch (err) {
        res.err = err;
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;