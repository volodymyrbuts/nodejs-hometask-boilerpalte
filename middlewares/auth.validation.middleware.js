const AuthService = require('../services/authService');

const authValid = (req, res) => {

    if (AuthService.login({ email: req.body.email }) === undefined) {
        throw (res.status(404).json({
            error: true,
            message: 'Wrong email'
        }))
    }

    if (req.body.password === undefined || AuthService.login({ email: req.body.email }).password != req.body.password) {
        throw (res.status(400).json({
            error: true,
            message: 'Wrong pass'
        }))
    }

    return res.json(AuthService.login({ email: req.body.email }))
}

exports.authValid = authValid