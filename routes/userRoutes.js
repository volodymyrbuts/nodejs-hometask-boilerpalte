const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const router = Router();

router.post('/', createUserValid, (req, res, next) => {
    try {
        res.data = UserService.create(req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        res.data = UserService.get()
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = UserService.search(req.params)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


router.delete('/:id', (req, res, next) => {
    try {
        res.data = UserService.delete(req.params.id)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        res.data = UserService.update(req.params.id, req.body)
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;