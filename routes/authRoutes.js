const { Router } = require('express');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { authValid } = require('../middlewares/auth.validation.middleware.js');
const router = Router();

router.post('/login', authValid, (res, next) => {
    try {
        res.data
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;